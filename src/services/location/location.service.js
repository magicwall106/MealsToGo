import camelize from "camelize";
import { locations } from "./location.mock";
export const locationRequest = (searchText) => {
  return new Promise((resolve, reject) => {
    const mockLocation = locations[searchText];
    if (mockLocation) {
      resolve(mockLocation);
    }
    reject(`Could not found location [${searchText}]`);
  });
};

export const locationTransform = (result) => {
  const formattedResponse = camelize(result);
  const { geometry = {} } = formattedResponse.results[0];
  const { lat, lng } = geometry.location;
  console.log({ lat, lng, viewport: geometry.viewport });
  return { lat, lng, viewport: geometry.viewport };
};
