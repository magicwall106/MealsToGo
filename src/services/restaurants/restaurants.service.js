import { mocks, mockImages } from "./mock";
import camelize from "camelize";

export const restaurantsRequest = (location) => {
  return new Promise((resolve, reject) => {
    const mock = mocks[location];
    if (!mock) {
      reject("No location found");
    }

    resolve(mock);
  });
};
export const restaurantsTransform = ({ results = [] }) => {
  const mappedResult = results.map((result) => {
    result.photos = result.photos.map((p) => {
      return mockImages[Math.ceil(Math.random() * (mockImages.length - 1))];
    });
    return {
      ...result,
      address: result.vicinity,
      isOpenNow: result.opening_hours && result.opening_hours.open_now,
      isClosedTemporarily: result.business_status === "CLOSED TEMPORARILY",
    };
  });

  return camelize(mappedResult);
};
