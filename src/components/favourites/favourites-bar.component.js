import { ScrollView, TouchableOpacity } from "react-native";
import React from "react";
import { MapCalloutWrapper } from "../../features/map/components/map-callout.component";
import styled from "styled-components";
import { Spacer } from "../utilities/spacer.component";
import { Text } from "../typography/text.component";

const FavoritesWrapper = styled.View`
  padding: 10px;
`;
export const FavouritesBar = ({ favourites, onNavigate }) => {
  if (!favourites.length) {
    return null;
  }

  return (
    <FavoritesWrapper>
      <Spacer position="bottom" size="medium">
        <Text variant="caption">Favourites</Text>
      </Spacer>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {favourites.map((restaurant) => {
          const key = restaurant.name;
          return (
            <Spacer key={key} position="left" size="medium">
              <TouchableOpacity
                style={{ height: 150 }}
                onPress={() => {
                  onNavigate("RestaurantsDetail", restaurant);
                }}
              >
                <MapCalloutWrapper restaurant={restaurant} />
              </TouchableOpacity>
            </Spacer>
          );
        })}
      </ScrollView>
    </FavoritesWrapper>
  );
};
