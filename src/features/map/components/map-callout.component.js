import React from "react";
import styled from "styled-components";
import { Spacer } from "../../../components/utilities/spacer.component";
import { Ionicons } from "@expo/vector-icons";
import { Text } from "../../../components/typography/text.component";
import { theme } from "../../../infrastructure/theme";
import { WebView } from "react-native-webview";
import { Platform } from "react-native";

const FavoriteIconView = styled.TouchableOpacity`
  position: absolute;
  z-index: 999;
  top: ${(props) => props.theme.space[0]};
  right: ${(props) => props.theme.space[1]};
`;
const MapCalloutView = styled.View`
  flex: 1;
  align-items: center;
  width: ${(props) => props.theme.sizes[4]};
  height: ${(props) => props.theme.sizes[4]};
`;
const RestaurantCalloutImageWrapper = styled.View`
  width: ${(props) => props.theme.sizes[4]};
  height: ${(props) => props.theme.sizes[4]};
`;
const RestaurantCalloutImage = styled.Image`
  width: 100%;
  height: 100%;
  border-radius: ${(props) => props.theme.sizes[0]};
`;

const RestaurantCalloutWebviewImage = styled(WebView)`
  flex: 1;
  padding: 30px;
  width: 100%;
  height: 100%;
  border-radius: ${(props) => props.theme.sizes[0]};
`;

const isAndroid = Platform.OS === "android";
export const MapCalloutWrapper = ({ restaurant, isMap }) => {
  const Image =
    isAndroid && isMap ? RestaurantCalloutWebviewImage : RestaurantCalloutImage;
  return (
    <MapCalloutView>
      <RestaurantCalloutImageWrapper>
        <FavoriteIconView>
          <Ionicons name="heart" size={40} color={theme.colors.ui.tertiary} />
        </FavoriteIconView>
        <Image
          source={{
            uri: restaurant.photos[0],
          }}
        />
      </RestaurantCalloutImageWrapper>
      <Spacer position="top" size="medium">
        <Text variant="caption">{restaurant.name}</Text>
      </Spacer>
    </MapCalloutView>
  );
};
