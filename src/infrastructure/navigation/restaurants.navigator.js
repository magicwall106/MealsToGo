import React from "react";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";

import { RestaurantScreen } from "../../features/restaurants/screens/restaurant.screen";
import { RestaurantsDetailScreen } from "../../features/restaurants/screens/restaurants-detail.screen";

const RestaurantStackNavigator = createStackNavigator();

export const RestaurantStack = () => {
  return (
    <RestaurantStackNavigator.Navigator
      screenOptions={{
        ...TransitionPresets.ModalPresentationIOS,
        headerShown: false,
      }}
    >
      <RestaurantStackNavigator.Screen
        name="Restaurants"
        component={RestaurantScreen}
      />
      <RestaurantStackNavigator.Screen
        name="RestaurantsDetail"
        component={RestaurantsDetailScreen}
      />
    </RestaurantStackNavigator.Navigator>
  );
};
