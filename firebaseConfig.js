import { initializeApp } from "firebase/app";

// Optionally import the services that you want to use
// import {...} from "firebase/database";
// import {...} from "firebase/firestore";
// import {...} from "firebase/functions";
// import {...} from "firebase/storage";

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyCFeQX7bpmcRR5wDxkV7UCqWCswZsfY2nY",
  authDomain: "mealstogo-e8eb6.firebaseapp.com",
  projectId: "mealstogo-e8eb6",
  storageBucket: "mealstogo-e8eb6.appspot.com",
  messagingSenderId: "123530919202",
  appId: "1:123530919202:web:0ea7a12c52f9ce512ef97b",
  measurementId: "G-ZP0Z0YDZQN",
};

export const app = initializeApp(firebaseConfig);

// For more information on how to access Firebase in your project,
// see the Firebase documentation: https://firebase.google.com/docs/web/setup#access-firebase
