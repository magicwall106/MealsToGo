import React from "react";
import { StatusBar as ExpoStatusBar } from "expo-status-bar";
import { ThemeProvider } from "styled-components/native";
import { theme } from "./src/infrastructure/theme";
import AuthenticationContextProvider from "./src/services/authentication/authentication.context";

import {
  useFonts as useFontLato,
  Lato_400Regular,
} from "@expo-google-fonts/lato";
import {
  useFonts as useFontOswald,
  Oswald_400Regular,
} from "@expo-google-fonts/oswald";
import { Navigation } from "./src/infrastructure/navigation";

export default function App() {
  let [latoFontLoaded] = useFontLato({
    Lato_400Regular,
  });

  let [oswaldFontLoaded] = useFontOswald({
    Oswald_400Regular,
  });

  if (!latoFontLoaded || !oswaldFontLoaded) {
    return null;
  }

  return (
    <>
      <ThemeProvider theme={theme}>
        <AuthenticationContextProvider>
          <Navigation />
        </AuthenticationContextProvider>
      </ThemeProvider>
      <ExpoStatusBar style="auto" />
    </>
  );
}
